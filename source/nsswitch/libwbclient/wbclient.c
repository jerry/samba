/*
   Unix SMB/CIFS implementation.

   Winbind client API

   Copyright (C) Gerald (Jerry) Carter 2007


   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Required Headers */

#include "nsswitch/winbind_nss_config.h"
#include "nsswitch/winbind_struct_protocol.h"
#include "wbclient.h"
#include <talloc.h>

#define BAIL_ON_WBC_ERROR(x)  do { if ((x) != WBC_ERR_SUCCESS) goto fail; } while(0);

/* From wb_common.c */

NSS_STATUS winbindd_request_response(int req_type,
				     struct winbindd_request *request,
				     struct winbindd_response *response);

/** @brief Free library allocated memory
 *
 * @param *p Pointer to free
 *
 * @return void
 **/

void wbcFreeMemory(void *p)
{
	talloc_free(p);
}


/** @brief Convert a binary SID to a character string
 *
 * @param sid           Binary Security Identifier
 * @param **sid_string  Resulting character string
 *
 * @return #wbcErr
 **/

wbcErr wbcSidToString(const struct wbcDomainSid *sid,
		      char **sid_string)
{
	wbcErr ret = WBC_ERR_UNKNOWN_FAILURE;	
	uint32_t id_auth;
	int i;
	char *tmp = NULL;	
	TALLOC_CTX *mem_ctx = NULL;

	if (!sid)
		return WBC_ERR_INVALID_SID;

	if ((mem_ctx=talloc_init("wbcSidToString")) == NULL )
		return WBC_ERR_NO_MEMORY;
		
	id_auth = sid->id_auth[5] +
		(sid->id_auth[4] << 8) +
		(sid->id_auth[3] << 16) +
		(sid->id_auth[2] << 24);

	tmp = talloc_asprintf(mem_ctx, "S-%d-%d", sid->sid_rev_num, id_auth );
	if (!tmp) {
		ret = WBC_ERR_NO_MEMORY;
		BAIL_ON_WBC_ERROR(ret);
	}

	for (i=0; i<sid->num_auths; i++) {
		char *tmp2 = 
		tmp2 = talloc_asprintf_append(tmp, "-%u", sid->sub_auths[i]);
		if (!tmp2) {
			ret = WBC_ERR_NO_MEMORY;			
			BAIL_ON_WBC_ERROR(ret);			
		}
		tmp = tmp2;		
	}

	if ((*sid_string=talloc_strdup(NULL, tmp)) == NULL ) {
		ret = WBC_ERR_NO_MEMORY;
		BAIL_ON_WBC_ERROR(ret);
	}

	ret = WBC_ERR_SUCCESS;

 fail:
	talloc_free(mem_ctx);

	return ret;	
}

/** @brief Convert a character string to a binary SID
 *
 * @param *str          Character string in the form of S-...
 * @param sid           Resulting binary SID
 *
 * @return #wbcErr
 **/

wbcErr wbcStringToSid(const char *str,
		      struct wbcDomainSid *sid)
{
	const char *p;
	char *q;
	uint32_t x;

	if (!sid)
		return WBC_ERR_INVALID_PARAM;

	/* Sanity check for either "S-" or "s-" */

	if (!str
	    || (str[0]!='S' && str[0]!='s')
	    || (str[1]!='-')
	    || (strlen(str)<2))
	{
		return WBC_ERR_INVALID_SID;
	}

	/* Get the SID revision number */

	p = str+2;
	x = (uint32_t)strtol(p, &q, 10);
	if (x==0 || !q || *q!='-')
		return WBC_ERR_INVALID_SID;
	sid->sid_rev_num = (uint8_t)x;

	/* Next the Identifier Authority.  This is stored in big-endian
	   in a 6 byte array. */

	p = q+1;
	x = (uint32_t)strtol(p, &q, 10);
	if (x==0 || !q || *q!='-')
		return WBC_ERR_INVALID_SID;
	sid->id_auth[5] = (x & 0x000000ff);
	sid->id_auth[4] = (x & 0x0000ff00) >> 8;
	sid->id_auth[3] = (x & 0x00ff0000) >> 16;
	sid->id_auth[2] = (x & 0xff000000) >> 24;
	sid->id_auth[1] = 0;
	sid->id_auth[0] = 0;

	/* now read the the subauthorities */

	p = q +1;
	sid->num_auths = 0;
	while (sid->num_auths < MAXSUBAUTHS) {
		if ((x=(uint32_t)strtoul(p, &q, 10)) == 0)
			break;
		sid->sub_auths[sid->num_auths++] = x;

		if (q && ((*q!='-') || (*q=='\0')))
			break;
		p = q + 1;
	}

	/* IF we ended early, then the SID could not be converted */

	if (q && *q!='\0')
		return WBC_ERR_INVALID_SID;

	return WBC_ERR_SUCCESS;
}



/** @brief Convert a domain and name to SID
 *
 * @param domain      Domain name (possibly "")
 * @param name        User or group name
 * @param *sid        Pointer to the resolved domain SID
 * @param *name_type  Pointet to the SID type
 *
 * @return #wbcErr
 *
 **/

wbcErr wbcLookupName(const char *domain,
		     const char *name,
		     struct wbcDomainSid *sid,
		     enum wbcSidType *name_type)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;
	wbcErr wbc_ret = WBC_ERR_UNKNOWN_FAILURE;

	if (!sid || !name_type)
		return WBC_ERR_INVALID_PARAM;

	/* Initialize request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	/* dst is already null terminated from the memset above */

	strncpy(request.data.name.dom_name, domain,
		sizeof(request.data.name.dom_name)-1);
	strncpy(request.data.name.name, name,
		sizeof(request.data.name.name)-1);

	result = winbindd_request_response(WINBINDD_LOOKUPNAME,
					   &request,
					   &response);
	if ( result != NSS_STATUS_SUCCESS) {
		goto fail;
	}

	wbc_ret = wbcStringToSid(response.data.sid.sid, sid);
	BAIL_ON_WBC_ERROR(wbc_ret);

	*name_type = (enum wbcSidType)response.data.sid.type;
	wbc_ret = WBC_ERR_SUCCESS;

 fail:
	return wbc_ret;
}

/** @brief Convert a SID to a domain and name
 *
 * @param *sid        Pointer to the domain SID to be resolved
 * @param domain      Resolved Domain name (possibly "")
 * @param name        Resolved User or group name
 * @param *name_type  Pointet to the resolved SID type
 *
 * @return #wbcErr
 *
 **/

wbcErr wbcLookupSid(const struct wbcDomainSid *sid,
		    char **domain,
		    char **name,
		    enum wbcSidType *name_type)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;
	wbcErr wbc_ret = WBC_ERR_UNKNOWN_FAILURE;
	char *sid_string = NULL;

	*domain = NULL;
	*name = NULL;

	/* Initialize request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	/* dst is already null terminated from the memset above */

	wbc_ret = wbcSidToString(sid, &sid_string);
	BAIL_ON_WBC_ERROR(wbc_ret);

	strncpy(request.data.sid, sid_string, sizeof(request.data.sid)-1);
	wbcFreeMemory(sid_string);

	/* Make request */

	result = winbindd_request_response(WINBINDD_LOOKUPSID,
					   &request,
					   &response);

	if (result != NSS_STATUS_SUCCESS) {
		goto fail;
	}

	/* Copy out result */

	if (domain != NULL) {
		if ((*domain = strdup(response.data.name.dom_name)) == NULL) {
			wbc_ret = WBC_ERR_NO_MEMORY;
			BAIL_ON_WBC_ERROR(wbc_ret);
		}
	}

	if (name != NULL) {
		if ((*name = strdup(response.data.name.name)) == NULL) {
			wbc_ret = WBC_ERR_NO_MEMORY;
			BAIL_ON_WBC_ERROR(wbc_ret);
		}
	}

	if (name_type)
		*name_type = (enum wbcSidType)response.data.name.type;

	wbc_ret = WBC_ERR_SUCCESS;

 fail:
	if (wbc_ret != WBC_ERR_SUCCESS) {
		if (*domain)
			free(*domain);
		if (*name)
			free(*name);
	}

	return wbc_ret;
}

/** @brief Convert a Windows SID to a Unix uid
 *
 * @param *sid        Pointer to the domain SID to be resolved
 * @param *puid       Pointer to the resolved uid_t value
 *
 * @return #wbcErr
 *
 **/

wbcErr wbcSidToUid(const struct wbcDomainSid *sid, uid_t *puid)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;
	char *sid_string = NULL;
	wbcErr wbc_ret = WBC_ERR_UNKNOWN_FAILURE;

	if (!puid)
		return 0;

	/* Initialize request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	wbc_ret = wbcSidToString(sid, &sid_string);
	BAIL_ON_WBC_ERROR(wbc_ret);

	strncpy(request.data.sid, sid_string, sizeof(request.data.sid)-1);
	wbcFreeMemory(sid_string);

	/* Make request */

	result = winbindd_request_response(WINBINDD_SID_TO_UID,
					   &request,
					   &response);

	/* Copy out result */

	if (result != NSS_STATUS_SUCCESS) {
		return WBC_ERR_UNKNOWN_FAILURE;
	}

	*puid = response.data.uid;
	wbc_ret = WBC_ERR_SUCCESS;

 fail:
	return wbc_ret;
}

/** @brief Convert a Unix uid to a Windows SID
 *
 * @param uid         Unix uid to be resolved
 * @param *sid        Pointer to the resolved domain SID
 *
 * @return #wbcErr
 *
 **/

wbcErr wbcUidToSid(uid_t uid, struct wbcDomainSid *sid)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;

	if (!sid)
		return false;

	/* Initialize request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	request.data.uid = uid;

	/* Make request */

	result = winbindd_request_response(WINBINDD_UID_TO_SID,
					   &request,
					   &response);

	/* Copy out result */

	if (result != NSS_STATUS_SUCCESS) {
		return WBC_ERR_UNKNOWN_FAILURE;
	}

        return wbcStringToSid(response.data.sid.sid, sid);
}

/** @brief Convert a Windows SID to a Unix gid
 *
 * @param *sid        Pointer to the domain SID to be resolved
 * @param *pgid       Pointer to the resolved gid_t value
 *
 * @return #wbcErr
 *
 **/

wbcErr wbcSidToGid(const struct wbcDomainSid *sid, gid_t *pgid)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;
	wbcErr wbc_ret = WBC_ERR_UNKNOWN_FAILURE;
	char *sid_string = NULL;

	if (!pgid)
		return false;

	/* Initialize request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	wbc_ret = wbcSidToString(sid, &sid_string);
	BAIL_ON_WBC_ERROR(wbc_ret);

	strncpy(request.data.sid, sid_string, sizeof(request.data.sid)-1);
	wbcFreeMemory(sid_string);

	/* Make request */

	result = winbindd_request_response(WINBINDD_SID_TO_GID,
					   &request,
					   &response);

	/* Copy out result */

	if (result != NSS_STATUS_SUCCESS) {
		return WBC_ERR_UNKNOWN_FAILURE;
	}

	*pgid = response.data.gid;
	wbc_ret = WBC_ERR_SUCCESS;

 fail:
	return wbc_ret;
}

/** @brief Convert a Unix uid to a Windows SID
 *
 * @param gid         Unix gid to be resolved
 * @param *sid        Pointer to the resolved domain SID
 *
 * @return #wbcErr
 *
 **/

wbcErr wbcGidToSid(gid_t gid, struct wbcDomainSid *sid)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;

	if (!sid)
		return false;

	/* Initialize request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	request.data.gid = gid;

	/* Make request */

	result = winbindd_request_response(WINBINDD_GID_TO_SID,
					   &request,
					   &response);


	/* Copy out result */

	if (result != NSS_STATUS_SUCCESS) {
		return WBC_ERR_UNKNOWN_FAILURE;
	}

        return wbcStringToSid(response.data.sid.sid, sid);
}

/** @brief Ping winbindd to see if the daemon is running
 *
 * @return #wbcErr
 **/

wbcErr wbcPing(void)
{
	NSS_STATUS result;

	result = winbindd_request_response(WINBINDD_PING, NULL, NULL);

	if (result != NSS_STATUS_SUCCESS)
		return WBC_ERR_UNKNOWN_FAILURE;
	
	return WBC_ERR_SUCCESS;	
}

/** @brief Lookup the current status of a trusted domain
 *
 * @param domain      Domain to query
 * @param *info       Pointer to returned domain_info struct
 *
 * @return #wbcErr
 *
 * The char* members of the struct wbcDomainInfo* are malloc()'d
 * and it the the responsibility of the caller to free the members
 * before  discarding the struct.
 *
 **/

/**********************************************************************
 result == NSS_STATUS_UNAVAIL: winbind not around
 result == NSS_STATUS_NOTFOUND: winbind around, but domain missing

 Due to a bad API NSS_STATUS_NOTFOUND is returned both when winbind_off 
 and when winbind return WINBINDD_ERROR. So the semantics of this 
 routine depends on winbind_on. Grepping for winbind_off I just 
 found 3 places where winbind is turned off, and this does not conflict 
 (as far as I have seen) with the callers of is_trusted_domains.   

 --Volker
**********************************************************************/

wbcErr wbcDomainInfo(const char *domain, struct wbcDomainInfo *info)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;
	wbcErr ret;
	
	if (!domain || !info)
		return WBC_ERR_INVALID_PARAM;

	ZERO_STRUCT(*info);
	
	/* Initialize request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	strncpy(request.domain_name, domain, sizeof(request.domain_name)-1);	

	result = winbindd_request_response(WINBINDD_DOMAIN_INFO,
					   &request,
					   &response);

	/* Copy out result */

	if (result != NSS_STATUS_SUCCESS) {
		switch(result){
		case NSS_STATUS_UNAVAIL:
			return WBC_ERR_WINBIND_NOT_AVAILABLE;
			break;
		case NSS_STATUS_NOTFOUND:
			return WBC_ERR_DOMAIN_NOT_FOUND;
			break;			
		default:
			return WBC_ERR_UNKNOWN_FAILURE;
		}		
	}

	info->short_name = strdup(response.data.domain_info.name);
	info->dns_name   = strdup(response.data.domain_info.alt_name);
	if (!info->short_name || !info->dns_name) {		
		ret = WBC_ERR_NO_MEMORY;
		BAIL_ON_WBC_ERROR(ret);
	}	
	
	ret = wbcStringToSid(response.data.domain_info.sid, &info->sid);
	BAIL_ON_WBC_ERROR(ret);
	
	if (response.data.domain_info.native_mode)
		info->flags |= WBC_DOMINFO_NATIVE;
	if (response.data.domain_info.active_directory)
		info->flags |= WBC_DOMINFO_AD;
	if (response.data.domain_info.primary)
		info->flags |= WBC_DOMINFO_PRIMARY;

	ret = WBC_ERR_SUCCESS;	

 fail:
	if (ret != WBC_ERR_SUCCESS) {
		if (info->short_name)
			free(info->short_name);
		if (info->dns_name)
			free(info->dns_name);
	}
	
	return ret;	
}

/** @brief Translate a collection of RIDs within a domain to names
 *
 **/

wbcErr wbcLookupRids(struct wbcDomainSid *dom_sid, 
		     int num_rids, 
		     uint32_t *rids,
		     const char **domain_name,
		     const char ***names, 
		     enum wbcSidType **types)
{
	size_t i, len, ridbuf_size;	
	char *ridlist;
	char *p;
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;
	char *sid_string = NULL;	
	wbcErr ret;

	if (num_rids == 0) {
		return WBC_ERR_INVALID_PARAM;
	}

	/* Initialise request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	ret = wbcSidToString(dom_sid, &sid_string);
	BAIL_ON_WBC_ERROR(ret);

	strncpy(request.data.sid, sid_string, sizeof(request.data.sid)-1);
	wbcFreeMemory(sid_string);

	/* Even if all the Rids were of maximum 32bit values,
	   we would only have 11 bytes per rid in the final array
	   ("4294967296" + \n).  Add one more byte for the 
	   terminating '\0' */

	ridbuf_size = (sizeof(char)*11) * num_rids + 1;
	if ((ridlist = malloc(ridbuf_size)) == NULL)
		return WBC_ERR_NO_MEMORY;
	memset(ridlist, 0x0, ridbuf_size);	

	len = 0;
	for (i=0; i<num_rids && (len-1)>0; i++) {
		char ridstr[12];
		
		len = strlen(ridlist);
		p = ridlist + len;
		
		snprintf( ridstr, sizeof(ridstr)-1, "%u\n", rids[i]);
		strncat(p, ridstr, ridbuf_size-len-1);
	}

	request.extra_data.data = ridlist;
	request.extra_len = strlen(ridlist)+1;

	result = winbindd_request_response(WINBINDD_LOOKUPRIDS,
					   &request, &response);

	free(ridlist);	

	if (result != NSS_STATUS_SUCCESS) {
		return WBC_ERR_UNKNOWN_FAILURE;
	}

	*domain_name = strdup(response.data.domain_name);

	*names = (const char**)malloc(sizeof(char*) * num_rids);
	*types = (enum wbcSidType*)malloc(sizeof(enum wbcSidType) * num_rids);

	if (!*names || !*types) {
		ret = WBC_ERR_NO_MEMORY;
		BAIL_ON_WBC_ERROR(ret);
	}
	
	p = (char *)response.extra_data.data;

	for (i=0; i<num_rids; i++) {
		char *q;

		if (*p == '\0') {
			ret = WCB_INVALID_RESPONSE;
			BAIL_ON_WBC_ERROR(ret);
		}
			
		(*types)[i] = (enum wbcSidType)strtoul(p, &q, 10);

		if (*q != ' ') {
			ret = WCB_INVALID_RESPONSE;
			BAIL_ON_WBC_ERROR(ret);
		}

		p = q+1;

		if ((q = strchr(p, '\n')) == NULL) {
			ret = WCB_INVALID_RESPONSE;
			BAIL_ON_WBC_ERROR(ret);
		}

		*q = '\0';

		(*names)[i] = strdup(p);

		p = q+1;
	}

	if (*p != '\0') {
		ret = WCB_INVALID_RESPONSE;
		BAIL_ON_WBC_ERROR(ret);
	}

	free(response.extra_data.data);

	ret = WBC_ERR_SUCCESS;

 fail:
	if (ret != WBC_ERR_SUCCESS) {
		if (*domain_name)
			free(*domain_name);
		if (*names)
			free(*names);
		if (*types)
			free(*types);
	}

	return ret;
}

/** @brief Obtain a new uid from Winbind
 *
 * @param *puid      *pointer to the allocated uid
 *
 * @return #wbcErr
 **/

wbcErr wbcAllocateUid(uid_t *puid)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;

	if (!puid)
		return WBC_ERR_INVALID_PARAM;
	
	/* Initialise request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	/* Make request */

	result = winbindd_request_response(WINBINDD_ALLOCATE_UID,
					   &request, &response);

	if (result != NSS_STATUS_SUCCESS)
		return WBC_ERR_UNKNOWN_FAILURE;

	/* Copy out result */
	*puid = response.data.uid;

	return WBC_ERR_SUCCESS;
}

/** @brief Obtain a new gid from Winbind
 *
 * @param *pgid      Pointer to the allocated gid
 *
 * @return #wbcErr
 **/

wbcErr wbcAllocateGid(uid_t *pgid)
{
	struct winbindd_request request;
	struct winbindd_response response;
	NSS_STATUS result;

	if (!pgid)
		return WBC_ERR_INVALID_PARAM;
	
	/* Initialise request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	/* Make request */

	result = winbindd_request_response(WINBINDD_ALLOCATE_GID,
					   &request, &response);

	if (result != NSS_STATUS_SUCCESS)
		return WBC_ERR_UNKNOWN_FAILURE;

	/* Copy out result */
	*pgid = response.data.gid;

	return WBC_ERR_SUCCESS;
}

