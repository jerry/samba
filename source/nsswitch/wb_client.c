/* 
   Unix SMB/CIFS implementation.

   winbind client code

   Copyright (C) Tim Potter 2000
   Copyright (C) Andrew Tridgell 2000
   
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.
   
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "nsswitch/winbind_nss.h"
#include "libwbclient/wbclient.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_WINBIND

NSS_STATUS winbindd_request_response(int req_type,
                                 struct winbindd_request *request,
                                 struct winbindd_response *response);

/* Call winbindd to convert SID to uid */

bool winbind_sids_to_unixids(struct id_map *ids, int num_ids)
{
	struct winbindd_request request;
	struct winbindd_response response;
	int result;
	DOM_SID *sids;
	int i;

	/* Initialise request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	request.extra_len = num_ids * sizeof(DOM_SID);

	sids = (DOM_SID *)SMB_MALLOC(request.extra_len);
	for (i = 0; i < num_ids; i++) {
		sid_copy(&sids[i], ids[i].sid);
	}

	request.extra_data.data = (char *)sids;
	
	/* Make request */

	result = winbindd_request_response(WINBINDD_SIDS_TO_XIDS, &request, &response);

	/* Copy out result */

	if (result == NSS_STATUS_SUCCESS) {
		struct unixid *wid = (struct unixid *)response.extra_data.data;
		
		for (i = 0; i < num_ids; i++) {
			if (wid[i].type == -1) {
				ids[i].status = ID_UNMAPPED;
			} else {
				ids[i].status = ID_MAPPED;
				ids[i].xid.type = wid[i].type;
				ids[i].xid.id = wid[i].id;
			}
		}
	}

	SAFE_FREE(request.extra_data.data);
	SAFE_FREE(response.extra_data.data);

	return (result == NSS_STATUS_SUCCESS);
}

bool winbind_idmap_dump_maps(TALLOC_CTX *memctx, const char *file)
{
	struct winbindd_request request;
	struct winbindd_response response;
	int result;

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	request.extra_data.data = SMB_STRDUP(file);
	request.extra_len = strlen(request.extra_data.data) + 1;

	result = winbindd_request_response(WINBINDD_DUMP_MAPS, &request, &response);

	SAFE_FREE(request.extra_data.data);
	return (result == NSS_STATUS_SUCCESS);
}

bool winbind_set_mapping(const struct id_map *map)
{
	struct winbindd_request request;
	struct winbindd_response response;
	int result;

	/* Initialise request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	/* Make request */

	request.data.dual_idmapset.id = map->xid.id;
	request.data.dual_idmapset.type = map->xid.type;
	sid_to_string(request.data.dual_idmapset.sid, map->sid);

	result = winbindd_request_response(WINBINDD_SET_MAPPING, &request, &response);

	return (result == NSS_STATUS_SUCCESS);
}

bool winbind_set_uid_hwm(unsigned long id)
{
	struct winbindd_request request;
	struct winbindd_response response;
	int result;

	/* Initialise request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	/* Make request */

	request.data.dual_idmapset.id = id;
	request.data.dual_idmapset.type = ID_TYPE_UID;

	result = winbindd_request_response(WINBINDD_SET_HWM, &request, &response);

	return (result == NSS_STATUS_SUCCESS);
}

bool winbind_set_gid_hwm(unsigned long id)
{
	struct winbindd_request request;
	struct winbindd_response response;
	int result;

	/* Initialise request */

	ZERO_STRUCT(request);
	ZERO_STRUCT(response);

	/* Make request */

	request.data.dual_idmapset.id = id;
	request.data.dual_idmapset.type = ID_TYPE_GID;

	result = winbindd_request_response(WINBINDD_SET_HWM, &request, &response);

	return (result == NSS_STATUS_SUCCESS);
}
